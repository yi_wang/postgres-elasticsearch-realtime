# Postgres->elasticsearch realtime update proof-of-concept

This is a proof-of-concept rig showing how to propogate changes
updates in PostgreSQL database to an elasticsearch cluster in near
realtime.  This consists of two parts; a
[Vagrant](http://www.vagrantup.com/) VM that bootstraps standard
[PostgreSQL](http://www.postgresql.org/) and
[elasticsearch](http://www.elasticsearch.org/) instances, and a
[Clojure](http://clojure.org/) proof of concept implementation and
example test that propogates updates from Postgres to elasticsearch.

Note that this could also be converted into an elasticsearch
[River](http://www.elasticsearch.org/guide/en/elasticsearch/rivers/current/)
plugin.

For more details see [this blog post](http://haltcondition.net/TBD).

## Usage

To run the proof of concept test, do the following:

1. Install [VirtualBox](https://www.virtualbox.org/).
1. Install [Vagrant](http://www.vagrantup.com/).
1. Install [Ansible](https://github.com/ansible/ansible) (used for VM setup).
1. Install [Leiningen](https://github.com/technomancy/leiningen) (Clojure build/run tool).
1. Start the virtual machine with `vagrant up`
    * This starts the VM and installs the Postgres and ES.
	* See `provision-elastic-psql.yml` for the Ansible playbook.
1. Build and run the proof-of-concept test with `lein test`.

## License

Copyright © 2014 Steve Smith (tarkasteve@gmail.com)

This work is intended to be educational and is hereby placed in the
public domain, or under the Creative Commons CC0 1.0 Universal license
as appropriate. However attribution would be appreciated where
reasonable.
