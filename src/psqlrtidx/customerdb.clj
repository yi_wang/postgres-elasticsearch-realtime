(ns psqlrtidx.customerdb
  (:import com.impossibl.postgres.api.jdbc.PGNotificationListener)
  (:require [clojure.java.jdbc :as jdbc]
            [clj-dbcp.core :as dbcp]))

(def db-spec {:subprotocol "pgsql"
              :classname "com.impossibl.postgres.jdbc.PGDriver"
              :subname "//psql:5432/customer"
              :user "customer"
              :password "customer"})


(defn cursor-query 
  ([dbconn sql]
     (cursor-query dbconn sql 100))

  ([dbconn sql fetch-size]
     ;; Must disable autocommit for cursor queries and use a :forward-only prepared statement
     (.setAutoCommit (jdbc/get-connection dbconn) false)
     (jdbc/prepare-statement (jdbc/get-connection dbconn) sql
                             :result-type :forward-only
                             :concurrency :read-only
                             :fetch-size fetch-size)))


(defn each-account [fn]
  (jdbc/with-db-connection [dbconn db-spec]
    (let [sql "SELECT * FROM accountdetails"
          stmt (cursor-query dbconn sql)]
      (jdbc/query dbconn [stmt] :row-fn fn))))


(defn dispatch-account [fn id]
  (println "Performing update of" id)

  (jdbc/with-db-connection [dbconn db-spec]
    (jdbc/with-db-transaction [tx dbconn]
      (try
        ;; SELECT ... FOR UPDATE on the updated account log to handle concurrent listeners
        (jdbc/execute! dbconn ["SELECT id FROM updatedaccounts WHERE id=? FOR UPDATE" id])

        (jdbc/query dbconn ["SELECT * FROM account WHERE id=?" id] :row-fn fn)
        (jdbc/execute! dbconn ["DELETE FROM updatedaccounts WHERE id=?" id])

        (catch Exception e 
          (println e))))))


(defn dispatch-account-row [fn {id :id}]
  (dispatch-account fn id))


(defn each-pending [fn]
  (println "Start pending run")
  (jdbc/with-db-connection [dbconn db-spec]
    (println "DOING RUN")
    (let [sql "SELECT id FROM updatedaccounts"
          stmt (cursor-query dbconn sql)]
      ;; For each updated account
      (jdbc/query dbconn [stmt]
                  :row-fn (partial dispatch-account-row fn)))))


(defn notification-listen [rowfn]
  (println "connecting")
  (println "creating listener")
  (let [pgconn (jdbc/get-connection db-spec)
        listener (proxy [PGNotificationListener] []
                   (notification [pid channel payload]
                     (println "Received notification for" channel "for" payload)
                     (dispatch-account rowfn payload)
                     (println "Dispatch complete")))]

    (println "Registering listener")
    (.addNotificationListener pgconn listener)

    (println "Enabling notify")
    (jdbc/execute! {:connection pgconn} ["LISTEN accountupdate"])
    
    {:dbconn pgconn, :listener listener}))
